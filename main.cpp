#include <iostream>
#include <fstream>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <TGUI/TGUI.hpp>

#include <Box2D/Box2D.h>

#include "thirdparty/json.hpp"
#include "thirdparty/polypartition.hpp"

#include "src/ship.hpp"
#include "src/ship_components/ship_component_def.hpp"
#include "src/ship_components/ship_component.hpp"
#include "src/ship_components/engine_component_def.hpp"
#include "src/ship_components/engine_component.hpp"
#include "src/ship_components/thrusters_component_def.hpp"
#include "src/ship_components/thrusters_component.hpp"
#include "src/ship_components/power_source_component_def.hpp"
#include "src/ship_components/power_source_component.hpp"
#include "src/box2d/physics_object_stats.hpp"
#include "src/renderers/game_manager_renderer.hpp"
#include "src/utils/utils.hpp"
#include "src/game_manager.hpp"
#include "src/texture_manager.hpp"
#include "src/hardpoint.hpp"
#include "src/startup.hpp"
#include "src/background.hpp"
#include "src/serialisers/json_serialiser.hpp"
#include "src/polygon.hpp"

using json = nlohmann::json;

void login(tgui::EditBox::Ptr username, tgui::EditBox::Ptr password)
{
    std::cout << "Username: " << username->getText().toAnsiString() << std::endl;
    std::cout << "Password: " << password->getText().toAnsiString() << std::endl;
}

int main()
{
    // create the window
    sf::RenderWindow window(sf::VideoMode(1920, 1080), "Space Doggo");
    sf::View view({0, 0, 1920, 1080});
    window.setView(view);

    tgui::Gui gui(window);

/*
    // Create the username edit box
    // Similar to the picture, we set a relative position and size
    // In case it isn't obvious, the default text is the text that is displayed when the edit box is empty
    auto editBoxUsername = tgui::EditBox::create();
    editBoxUsername->setSize({"66.67%", "12.5%"});
    editBoxUsername->setPosition({"16.67%", "16.67%"});
    editBoxUsername->setDefaultText("Username");
    gui.add(editBoxUsername);

    // Create the password edit box
    // We copy the previous edit box here and keep the same size
    auto editBoxPassword = tgui::EditBox::copy(editBoxUsername);
    editBoxPassword->setPosition({"16.67%", "41.6%"});
    editBoxPassword->setPasswordCharacter('*');
    editBoxPassword->setDefaultText("Password");
    gui.add(editBoxPassword);

    // Create the login button
    auto button = tgui::Button::create("Login");
    button->setSize({"50%", "16.67%"});
    button->setPosition({"25%", "70%"});
    gui.add(button);

    // Call the login function when the button is pressed and pass the edit boxes that we created as parameters
    button->connect("pressed", login, editBoxUsername, editBoxPassword);
    */

    std::vector<sf::Vector2f> points = {
        {113, 22}, {143, 22}, {166, 47}, {166, 83}, {200, 133}, {225, 136}, {225, 207}, {218, 232}, {183, 232},
        {146, 242}, {109, 242}, {69, 232}, {35, 232}, {28, 207}, {28, 136}, {59, 133}, {87, 83}, {87, 47}
    };

    doggo::Polygon testPoly(points);

    b2World world(b2Vec2_zero);

    doggo::GameManager gameManager(window);
    gameManager.textureManager.rootFolder = "../data/textures/";

    doggo::GameManagerRenderer gameManagerRenderer(gameManager);
    gameManager.setRenderer(&gameManagerRenderer);

    doggo::Startup::loadTextures(gameManager);

    auto backgroundTexture = gameManager.textureManager.texture("background");
    auto backgroundTexture2 = gameManager.textureManager.texture("background2");
    doggo::Background background(*backgroundTexture, view, 0.1f, {101, 29}, sf::Color(255, 255, 255, 80));
    doggo::Background background2(*backgroundTexture2, view, 0.2f, {33, 113}, sf::Color(255, 255, 255, 160));

/*
    auto engineDef = std::make_unique<doggo::EngineComponentDef>("engine1", "Engine", 3.0f, 1.0f, 10000.0f);
    auto thrustersDef = std::make_unique<doggo::ThrustersComponentDef>("thrusters1", "Thrusters", 2.0f, 1.0f, 7500.0f);
    auto powerSourceDef = std::make_unique<doggo::PowerSourceComponentDef>("powersource1", "Power Source", 1.0f, 5.0f);

    auto shipCompDefManager = std::make_unique<doggo::ShipComponentDefManager>();
    shipCompDefManager->registerDef(std::move(engineDef));
    shipCompDefManager->registerDef(std::move(thrustersDef));
    shipCompDefManager->registerDef(std::move(powerSourceDef));

    gameManager.setShipComponentDefManager(std::move(shipCompDefManager));
    */
    std::ifstream inputJsonStream("../data/components/ship_components.json", std::ifstream::in);
    if (inputJsonStream.good())
    {
        auto inputJson = json::parse(inputJsonStream);
        auto shipCompDefManager = doggo::JsonSerialiser::toShipComponentDefManager(inputJson);
        gameManager.setShipComponentDefManager(std::move(shipCompDefManager));
    }
    else
    {
        std::cout << "Unable to find ship_components.json" << std::endl;
        return -1;
    }

    auto shipPtr = new doggo::Ship(gameManager.nextId(), "FirstShip", "ship1", {5.0f, {-128.0f, -128.0f}, 2.0f, testPoly});
    doggo::Ship &ship = *shipPtr;
    gameManager.registerGameObject(shipPtr);
    ship.createPhysics(world);

    auto engineGet = gameManager.tryGetShipComponentDef<doggo::EngineComponentDef>("engine1");
    if (engineGet.first)
    {
        auto &hardpoint = ship.addHardpoint("engine", {0, 0}, 1.0f);
        hardpoint.setEquipped(new doggo::EngineComponent(3, *engineGet.second));
    }

    auto powerSourceGet = gameManager.tryGetShipComponentDef<doggo::PowerSourceComponentDef>("powerSource1");
    if (powerSourceGet.first)
    {
        auto &hardpoint2 = ship.addHardpoint("power", {0, 0}, 1.0f);
        hardpoint2.setEquipped(new doggo::PowerSourceComponent(5, *powerSourceGet.second));
    }

    auto thrustersGet = gameManager.tryGetShipComponentDef<doggo::ThrustersComponentDef>("thrusters1");
    if (thrustersGet.first)
    {
        auto &hardpoint3 = ship.addHardpoint("thrusters", {0, 0}, 1.0f);
        hardpoint3.setEquipped(new doggo::ThrustersComponent(6, *thrustersGet.second));
    }

    auto boxPtr = new doggo::Ship(gameManager.nextId(), "Box", "ship1", {5000.0f, {-128.0f, -128.0f}, 2.0f, testPoly, {-500, 0}, 0});
    doggo::Ship &box = *boxPtr;
    gameManager.registerGameObject(boxPtr);
    box.createPhysics(world);

    sf::Clock clock;

    //auto shipJson = doggo::JsonSerialiser::fromShip(ship);
    //auto newShip = doggo::JsonSerialiser::toShip(shipJson);
    //newShip->shipName = "JsonShip";
    //newShip->createPhysics(world);

    //gameManager.registerShip(std::move(newShip));

    // run the program as long as the window is open
    while (window.isOpen())
    {
        sf::Time elapsed = clock.restart();
        float dt = elapsed.asSeconds();

        // check all the window's events that were triggered since the last iteration of the loop
        sf::Event event;
        while (window.pollEvent(event))
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed)
            {
                window.close();
                continue;
            }

            // catch the resize events
            if (event.type == sf::Event::Resized)
            {
                // update the view to the new size of the window
                view.setCenter(.0f, 0.0f);
                view.setSize(event.size.width, event.size.height);

                window.setView(view);

                view.setCenter(event.size.width * 0.5f, event.size.height * 0.5f);
                gui.setView(view);
                continue;
            }

            gui.handleEvent(event);
        }

        ship.rotate(doggo::NoRotate);
        ship.move(doggo::NoMove);
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        {
            ship.rotate(doggo::CounterClockwise);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            ship.rotate(doggo::Clockwise);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        {
            ship.move(doggo::Forwards);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        {
            ship.move(doggo::Backwards);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
        {
            ship.move(doggo::Left);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
        {
            ship.move(doggo::Right);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
        {
            window.close();
            break;
        }

        gameManager.update(dt);
        world.Step(1.0f / 60.0f, 6, 2);
        gameManagerRenderer.update(dt);
        background.update(dt);
        background2.update(dt);

        auto pos = ship.physicsObj.body->GetPosition();
        view.setCenter(doggo::Utils::fromBox2d(pos));

        // clear the window with black color
        window.clear(sf::Color::Black);

        window.setView(sf::View(sf::Vector2f(), view.getSize()));
        window.draw(background.sprite);
        window.draw(background2.sprite);

        window.setView(view);
        window.draw(gameManagerRenderer);

        gui.draw();

        // end the current frame
        window.display();

        sf::sleep(sf::milliseconds(10));
    }

    return 0;
}