#include "background.hpp"

#include <iostream>

namespace doggo
{
    Background::Background(const sf::Texture &texture, const sf::View &view, float layerMoveFactor, const sf::Vector2i offset,
        const sf::Color colour) :
        sprite(texture),
        view(view),
        layerMoveFactor(layerMoveFactor),
        offset(offset)
    {
        this->sprite.setScale(2.0f, 2.0f);
        this->sprite.setColor(colour);
        this->update(0.0f);
    }

    void Background::update(float dt)
    {
        auto size = this->view.getSize();
        auto center = this->view.getCenter();
        this->sprite.setOrigin(size * 0.25f);
        this->sprite.setTextureRect(sf::IntRect((sf::Vector2i)(center * this->layerMoveFactor) + offset, (sf::Vector2i)size));
    }
} // doggo
