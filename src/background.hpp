#pragma once

#include <SFML/Graphics.hpp>

#include "non_copyable.hpp"

namespace doggo
{
    class Background : public NonCopyable
    {
        public:
            // Fields
            sf::Sprite sprite;
            const sf::View &view;
            const float layerMoveFactor;
            const sf::Vector2i offset;

            // Constructor
            Background(const sf::Texture &texture, const sf::View &view, float layerMoveFactor, const sf::Vector2i offset,
                const sf::Color colour);

            // Methods
            void update(float dt);
    };
} // doggo