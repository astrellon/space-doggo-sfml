#include "physics_object.hpp"

#include "../utils/utils.hpp"

#include <iostream>

namespace doggo
{
    PhysicsObject::PhysicsObject(float mass, const sf::Vector2f &partitionOffset, float partitionScale, const Polygon &polygon):
        physicsStats(mass, partitionOffset, partitionScale, polygon),
        partitionedPolygon(polygon.offset(partitionOffset, partitionScale).partition()),
        body(nullptr),
        world(nullptr)
    {

    }
    PhysicsObject::PhysicsObject(const PhysicsObjectStats &physicsStats) :
        physicsStats(physicsStats),
        partitionedPolygon(physicsStats.polygon.offset(physicsStats.offset, physicsStats.scale).partition()),
        body(nullptr),
        world(nullptr)
    {

    }

    PhysicsObject::~PhysicsObject()
    {
        this->detachFromWorld();
    }

    void PhysicsObject::attachToWorld(b2World *world)
    {
        this->detachFromWorld();

        this->world = world;

        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;

        this->body = world->CreateBody(&bodyDef);

        for (auto &polygon : this->partitionedPolygon)
        {
            if (!polygon.isValidBox2dPolygon())
            {
                continue;
            }

            b2PolygonShape bodyShape;
            std::vector<b2Vec2> points;
            points.reserve(polygon.points.size());

            for (auto &point : polygon.points)
            {
                points.push_back(Utils::toBox2d(point));
            }

            bodyShape.Set(&points[0], points.size());

            this->body->CreateFixture(&bodyShape, 1);
        }

        b2MassData massData;
        this->body->GetMassData(&massData);

        // Update moment of inertia with new mass data.
        auto oldMass = massData.mass;
        massData.mass = this->physicsStats.mass;
        massData.I *= (this->physicsStats.mass / oldMass);

        this->body->SetMassData(&massData);
        this->body->SetTransform(Utils::toBox2d(this->physicsStats.position), this->physicsStats.rotation);
    }

    void PhysicsObject::detachFromWorld()
    {
        if (this->world != nullptr)
        {
            if (this->body != nullptr)
            {
                this->world->DestroyBody(this->body);
            }
        }

        this->world = nullptr;
        this->body = nullptr;
    }

} // doggo
