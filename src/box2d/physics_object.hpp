#pragma once

#include <vector>

#include <SFML/System.hpp>
#include <Box2D/Box2D.h>

#include "../non_copyable.hpp"
#include "../polygon.hpp"

#include "physics_object_stats.hpp"

namespace doggo
{
    class PhysicsObject : public NonCopyable
    {
        public:
            // Fields
            const PhysicsObjectStats physicsStats;
            const std::vector<Polygon> partitionedPolygon;
            b2Body *body;
            b2World *world;

            // Constructor
            PhysicsObject(const PhysicsObjectStats &physicsStats);
            PhysicsObject(float mass, const sf::Vector2f &partitionOffset, float partitionScale, const Polygon &polygon);
            ~PhysicsObject();

            // Methods
            void attachToWorld(b2World *world);
            void detachFromWorld();

        private:

    };
} // doggo