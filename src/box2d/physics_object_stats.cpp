#include "physics_object_stats.hpp"

namespace doggo
{
    PhysicsObjectStats::PhysicsObjectStats(float mass, const sf::Vector2f &offset, float scale, const Polygon &polygon, const sf::Vector2f &position, float rotation):
        mass(mass),
        polygon(polygon),
        offset(offset),
        scale(scale),
        position(position),
        rotation(rotation)
    {

    }
} // doggo
