#pragma once

#include "../polygon.hpp"

namespace doggo
{
    class PhysicsObjectStats
    {
        public:
            // Fields
            const float mass;
            const Polygon polygon;
            const sf::Vector2f offset;
            const float scale;
            const sf::Vector2f position;
            const float rotation;

            // Constructor
            PhysicsObjectStats(float mass, const sf::Vector2f &offset, float scale, const Polygon &polygon, const sf::Vector2f &position = sf::Vector2f(), float rotation = 0.0f);

            // Methods
    };
} // doggo