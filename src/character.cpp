#include "character.hpp"

namespace doggo
{
    Character::Character(const std::string &name, sf::Texture &spriteTexture) :
        name(name),
        sprite(spriteTexture)
    {

    }

    void Character::update(float dt)
    {
    }
} // doggo
