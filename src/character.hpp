#pragma once

#include <SFML/Graphics.hpp>
#include <string>

#include "non_copyable.hpp"
#include "box2d/physics_object.hpp"

namespace doggo
{
    class Character : public NonCopyable
    {
        public:
            // Fields
            sf::Sprite sprite;
            const std::string name;

            // Constructor
            Character(const std::string &name, sf::Texture &spriteTexture);


            // Methods
            void update(float dt);
    };
} // doggo