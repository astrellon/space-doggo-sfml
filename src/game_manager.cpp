#include "game_manager.hpp"

#include <iostream>
#include <memory>

#include "renderers/game_manager_renderer.hpp"

namespace doggo
{
    int GameManager::currentId = 0;

    GameManager::GameManager(sf::RenderWindow &window) :
        window(window),
        renderer(nullptr),
        shipComponentDefManager(std::make_unique<ShipComponentDefManager>())
    {

    }

    bool GameManager::registerGameObject(std::unique_ptr<GameObject> gameObject)
    {
        if (this->doRegisterGameObject(gameObject.get()))
        {
            this->gameObjects.emplace(gameObject->id, std::move(gameObject));
            return true;
        }

        return false;
    }

    bool GameManager::registerGameObject(GameObject *gameObject)
    {
        if (this->doRegisterGameObject(gameObject))
        {
            this->gameObjects.emplace(gameObject->id, gameObject);
            return true;
        }

        return false;
    }

    void GameManager::setShipComponentDefManager(std::unique_ptr<ShipComponentDefManager> defManager)
    {
        this->shipComponentDefManager = std::move(defManager);
    }

    void GameManager::setRenderer(GameManagerRenderer *renderer)
    {
        this->renderer = renderer;
    }

    void GameManager::update(float dt)
    {
        for (auto &&pair : this->gameObjects)
        {
            pair.second->update(dt);
        }
    }

    void GameManager::updateGameObjectRenderer(GameObject &gameObject)
    {
        if (this->renderer == nullptr)
        {
            return;
        }

        this->renderer->addGameObject(gameObject);
    }

    bool GameManager::doRegisterGameObject(GameObject *gameObject)
    {
        auto id = gameObject->id;
        if (this->gameObjects.find(id) != this->gameObjects.end())
        {
            return false;
        }

        GameManager::checkId(gameObject->id);
        this->updateGameObjectRenderer(*gameObject);
        return true;
    }
} // doggo
