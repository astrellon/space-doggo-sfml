#pragma once

#include <map>

#include "ship_components/ship_component_def_manager.hpp"
#include "game_object.hpp"
#include "texture_manager.hpp"
#include "non_copyable.hpp"
#include "polygon.hpp"

namespace doggo
{
    class GameManagerRenderer;

    class GameManager : public NonCopyable
    {
        public:
            // Fields
            TextureManager textureManager;
            sf::RenderWindow &window;

            // Constructor
            GameManager(sf::RenderWindow &window);

            // Methods
            void update(float dt);

            bool registerGameObject(std::unique_ptr<GameObject> gameObject);
            bool registerGameObject(GameObject *gameObject);
            void setShipComponentDefManager(std::unique_ptr<ShipComponentDefManager> defManager);
            void setRenderer(GameManagerRenderer *renderer);

            template <class T>
            std::pair<bool, T *> tryGetShipComponentDef(const std::string &id) const
            {
                auto find = this->shipComponentDefManager->shipCompDefs.find(id);
                if (find == this->shipComponentDefManager->shipCompDefs.end())
                {
                    return {false, nullptr};
                }

                auto findType = dynamic_cast<T *>(find->second.get());
                if (findType == nullptr)
                {
                    return {false, nullptr};
                }

                return {true, findType};
            }

            static inline int nextId()
            {
                return ++currentId;
            }

            static void checkId(int id)
            {
                if (id > currentId)
                {
                    currentId = id;
                }
            }

        private:
            // Fields
            GameManagerRenderer *renderer;

            static int currentId;

            std::map<int, std::unique_ptr<GameObject>> gameObjects;
            std::unique_ptr<ShipComponentDefManager> shipComponentDefManager;

            void updateGameObjectRenderer(GameObject &gameObject);
            bool doRegisterGameObject(GameObject *gameObject);
    };
} // doggo
