#include "game_object.hpp"

namespace doggo
{
    GameObject::GameObject(int id, const PhysicsObjectStats &physicsStats) : 
        id(id), physicsObj(physicsStats) 
    { 

    }

    void GameObject::createPhysics(b2World &world)
    {
        this->physicsObj.attachToWorld(&world);
    }
} // doggo
