#pragma once

#include <SFML/System.hpp>
#include <Box2D/Box2D.h>

#include "box2d/physics_object_stats.hpp"
#include "box2d/physics_object.hpp"
#include "non_copyable.hpp"

namespace doggo
{
    enum RendererType
    {
        RenderUnknown, RenderShip, RenderPolygon
    };

    class GameObject : NonCopyable
    {
        public:
            // Fields
            const int id;
            PhysicsObject physicsObj;
        
            // Constructor
            GameObject(int id, const PhysicsObjectStats &physicsStats); 
        
            // Methods
            void createPhysics(b2World &world);

            virtual void update(float dt) { }
            virtual RendererType rendererType() { return RenderUnknown; }
    };
} // doggo