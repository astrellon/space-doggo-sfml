#include "hardpoint.hpp"

#include "ship.hpp"

namespace doggo
{
    Hardpoint::Hardpoint(const std::string &name, const sf::Vector2f &position, float size, Ship &parent) :
        name(name),
        position(position),
        size(size),
        parent(parent)
    {

    }

    void Hardpoint::setEquipped(ShipComponent *component)
    {
        this->equipped = std::unique_ptr<ShipComponent>(component);
        component->parent = this;
    }
    std::unique_ptr<ShipComponent> Hardpoint::unequip()
    {
        return std::move(this->equipped);
    }
} // doggo
