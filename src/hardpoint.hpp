#pragma once

#include <SFML/System.hpp>
#include <string>

#include "ship_components/ship_component.hpp"
#include "non_copyable.hpp"

namespace doggo
{
    class Ship;

    class Hardpoint : public NonCopyable
    {
        public:
            // Fields
            const std::string name;
            const sf::Vector2f position;
            const float size;
            Ship &parent;

            // Constructor
            Hardpoint(const std::string &name, const sf::Vector2f &position, float size, Ship &parent);

            // Methods
            inline ShipComponent *getEquipped() const
            {
                return this->equipped.get();
            }
            void setEquipped(ShipComponent *component);
            std::unique_ptr<ShipComponent> unequip();

        private:
            // Fields
            std::unique_ptr<ShipComponent> equipped;

            // Methods
    };
} // doggo