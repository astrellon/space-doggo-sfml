#pragma once

namespace doggo
{
    class NonCopyable
    {
        public:
            NonCopyable(NonCopyable const&) = delete;
            NonCopyable& operator=(NonCopyable const&) = delete;
            NonCopyable() {}
    };
} // doggo