#include "polygon.hpp"

#include <iostream>
#include <Box2D/Box2D.h>
#include "utils/utils.hpp"

namespace doggo
{
    Polygon::Polygon(const std::vector<sf::Vector2f> &points):
        points(points)
    {
    }

    Polygon::Polygon(const TPPLPoly &poly):
        points(convertPolygon(poly))
    {
    }

    Polygon Polygon::offset(const sf::Vector2f &move, float scale) const
    {
        std::vector<sf::Vector2f> newPoints;
        for (auto &point : this->points)
        {
            newPoints.emplace_back((point.x + move.x) * scale, (point.y + move.y) * scale);
        }

        return Polygon(newPoints);
    }

    std::vector<Polygon> Polygon::partition() const
    {
        TPPLPartition pp;
        TPPLPoly tpplPoly = convertPolygon(*this);

        TPPLPolyList polyResult;
        pp.Triangulate_EC(&tpplPoly, &polyResult);

        std::vector<Polygon> result;
        for (const auto &tp : polyResult)
        {
            result.emplace_back(tp);
        }

        return result;
    }

    bool Polygon::isValidBox2dPolygon() const
    {
        if (this->points.size() < 3)
        {
            return false;
        }

        // Perform welding and copy vertices into local buffer.
        b2Vec2 ps[b2_maxPolygonVertices];
        auto tempCount = 0;
        for (auto &point : this->points)
        {
            auto v = Utils::toBox2d(point);

            auto unique = true;
            for (auto j = 0; j < tempCount; ++j)
            {
                if (b2DistanceSquared(v, ps[j]) < ((0.5f * b2_linearSlop) * (0.5f * b2_linearSlop)))
                {
                    unique = false;
                    break;
                }
            }

            if (unique)
            {
                ps[tempCount++] = v;
            }
        }

        return tempCount >= 3;
    }

    float Polygon::area() const
    {
        // Initialze area
        auto area = 0.0;

        // Calculate value of shoelace formula
        auto num = this->points.size();
        auto j = num - 1;
        for (auto i = 0; i < num; i++)
        {
            auto &ipoint = this->points[i];
            auto &jpoint = this->points[j];
            area += (jpoint.x + ipoint.x) * (jpoint.y - ipoint.y);
            j = i;  // j is previous vertex to i
        }

        // Return absolute value
        return (float)abs(area * 0.5);
    }

    std::vector<sf::Vector2f> Polygon::convertPolygon(const TPPLPoly &poly)
    {
        std::vector<sf::Vector2f> p;
        p.reserve(poly.GetNumPoints());
        for (auto i = 0; i < poly.GetNumPoints(); i++)
        {
            const auto &point = poly.GetPoint(i);
            p.push_back({point.x, point.y});
        }

        return p;
    }

    TPPLPoly Polygon::convertPolygon(const Polygon &poly)
    {
        TPPLPoly result;
        result.Init(poly.points.size());
        result.SetOrientation(TPPL_CW);
        for (auto i = 0; i < poly.points.size(); i++)
        {
            const auto &point = poly.points[i];
            result[i] = {point.x, point.y};
        }
        return result;
    }
} // doggo
