#pragma once

#include <memory>
#include <vector>
#include <SFML/System.hpp>

#include "../thirdparty/polypartition.hpp"

namespace doggo
{
    class Polygon
    {
        public:
            // Fields
            const std::vector<sf::Vector2f> points;

            // Constructor
            Polygon(const std::vector<sf::Vector2f> &points);
            Polygon(const TPPLPoly &tpplPoly);

            // Methods
            Polygon offset(const sf::Vector2f &move, float scale) const;
            std::vector<Polygon> partition() const;
            bool isValidBox2dPolygon() const;

            float area() const;

        private:

            // Constructor

            // Static methods
            static std::vector<sf::Vector2f> convertPolygon(const TPPLPoly &poly);
            static TPPLPoly convertPolygon(const Polygon &poly);
    };
} // doggo