#pragma once

namespace doggo
{
    struct PowerNeeds
    {
        // Fields
        const float available;
        const float needed;

        // Constructor
        PowerNeeds(float available, float needed) :
            available(available),
            needed(needed)
        {

        }
    };
} // doggo
