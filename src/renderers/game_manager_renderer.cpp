#include "game_manager_renderer.hpp"

#include "ship_renderer.hpp"

namespace doggo
{
    GameManagerRenderer::GameManagerRenderer(const GameManager &gameManager) :
        gameManager(gameManager)
    {

    }

    void GameManagerRenderer::update(float dt)
    {
        for (auto &&pair : this->shipRenderers)
        {
            pair.second->timeUpdate(dt);
        }
    }

    void GameManagerRenderer::addGameObject(GameObject &gameObject)
    {
        auto rendererType = gameObject.rendererType();
        if (rendererType == RenderShip)
        {
            auto &ship = static_cast<Ship &>(gameObject);
            auto texture = this->gameManager.textureManager.texture(ship.textureName);
            this->shipRenderers.emplace(ship.id, new ShipRenderer(*texture, ship));
        }
    }

    void GameManagerRenderer::draw(sf::RenderTarget &target, sf::RenderStates states) const
    {
        for (auto &&pair : this->shipRenderers)
        {
            target.draw(*pair.second.get());
        }
    }
} // doggo
