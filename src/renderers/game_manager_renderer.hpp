#pragma once

#include <map>

#include <SFML/Graphics.hpp>

#include "../non_copyable.hpp"
#include "../game_manager.hpp"

#include "ship_renderer.hpp"

namespace doggo
{
    class GameManagerRenderer : public NonCopyable, public sf::Drawable
    {
        public:
            // Fields
            const GameManager &gameManager;
            std::map<int, std::unique_ptr<ShipRenderer>> shipRenderers;

            // Constructor
            GameManagerRenderer(const GameManager &gameManager);

            // Methods
            void update(float dt);

            void addGameObject(GameObject &gameObject);
            void removeGameObject(GameObject &gameObject);

            virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
    };
} // doggo