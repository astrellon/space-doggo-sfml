#include "game_object_renderer.hpp"

#include "../utils/utils.hpp"

namespace doggo
{
    GameObjectRenderer::GameObjectRenderer(const GameObject &gameObject) :
        gameObject(gameObject) 
    { 

    }

    void GameObjectRenderer::timeUpdate(float dt)
    {
        const auto &physicsObj = this->gameObject.physicsObj.body;
        const auto &pos = physicsObj->GetPosition();
        const auto &rotation = physicsObj->GetAngle();
        const auto &b2Position = Utils::fromBox2d(pos);
        const auto b2Rotation = rotation * RAD_TO_DEG;

        this->update(dt, b2Position, b2Rotation);
    }
} // doggo
