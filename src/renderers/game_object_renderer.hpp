#pragma once

#include <SFML/Graphics.hpp>

#include "../non_copyable.hpp"
#include "../game_object.hpp"

namespace doggo
{
    class GameObjectRenderer : public NonCopyable, public sf::Drawable
    {
        public:
            // Fields
            const GameObject &gameObject;
        
            // Constructor
            GameObjectRenderer(const GameObject &gameObject);
        
            // Methods
            void timeUpdate(float dt);
            virtual void update(float dt, const sf::Vector2f &b2Position, float b2Rotation) { }
    };
} // doggo