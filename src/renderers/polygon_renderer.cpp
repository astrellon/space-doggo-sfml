#include "polygon_renderer.hpp"

namespace doggo
{
    PolygonRenderer::PolygonRenderer(const Polygon &polygon)
    {
        this->createShape(polygon);
    }
    PolygonRenderer::PolygonRenderer(const std::vector<Polygon> &polygons)
    {
        this->createShapes(polygons);
    }

    void PolygonRenderer::update(const sf::Vector2f &position, float rotation)
    {
        for (auto &shape : this->shapes)
        {
            shape.setPosition(position);
            shape.setRotation(rotation);
        }
    }
    void PolygonRenderer::draw(sf::RenderTarget &target, sf::RenderStates states) const
    {
        for (auto &shape : this->shapes)
        {
            target.draw(shape);
        }
    }

    void PolygonRenderer::createShape(const Polygon &polygon)
    {
        sf::ConvexShape shape;
        shape.setPointCount(polygon.points.size());

        auto i = 0;
        for (auto &point : polygon.points)
        {
            shape.setPoint(i++, point);
        }

        shape.setFillColor(sf::Color::Transparent);
        shape.setOutlineColor(sf::Color::White);
        shape.setOutlineThickness(1);

        this->shapes.push_back(shape);
    }
    void PolygonRenderer::createShapes(const std::vector<Polygon> &polygons)
    {
        for (auto &polygon : polygons)
        {
            this->createShape(polygon);
        }
    }
} // doggo
