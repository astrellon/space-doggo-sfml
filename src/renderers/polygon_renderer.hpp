#pragma once

#include <SFML/Graphics.hpp>

#include "../non_copyable.hpp"
#include "../polygon.hpp"

namespace doggo
{
    class PolygonRenderer : public NonCopyable, public sf::Drawable
    {
        public:
            // Constructor
            PolygonRenderer(const Polygon &polygon);
            PolygonRenderer(const std::vector<Polygon> &polygons);

            // Methods
            void update(const sf::Vector2f &position, float rotation);
            virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;

        private:
            // Fields
            std::vector<sf::ConvexShape> shapes;

            // Methods
            void createShape(const Polygon &polygon);
            void createShapes(const std::vector<Polygon> &polygons);
    };
} // doggo