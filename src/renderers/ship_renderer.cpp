#include "ship_renderer.hpp"

#include "../utils/utils.hpp"

namespace doggo
{
    ShipRenderer::ShipRenderer(const sf::Texture &spriteTexture, const Ship &ship) :
        GameObjectRenderer(ship),
        sprite(spriteTexture),
        ship(ship),
        outlineRenderer(ship.physicsObj.partitionedPolygon)
    {
        this->sprite.setScale(2.0f, 2.0f);

        auto textureSize = this->sprite.getTexture()->getSize();
        this->sprite.setOrigin(textureSize.x / 2, textureSize.y / 2);
    }

    void ShipRenderer::update(float dt, const sf::Vector2f &b2Position, float b2Rotation)
    {
        this->sprite.setPosition(b2Position);
        this->sprite.setRotation(b2Rotation);

        this->outlineRenderer.update(b2Position, b2Rotation);
    }

    void ShipRenderer::draw(sf::RenderTarget &target, sf::RenderStates states) const
    {
        target.draw(this->sprite);
        target.draw(this->outlineRenderer);
    }
} // doggo
