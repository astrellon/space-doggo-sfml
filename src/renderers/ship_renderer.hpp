#pragma once

#include <SFML/Graphics.hpp>

#include "../non_copyable.hpp"
#include "../ship.hpp"

#include "polygon_renderer.hpp"
#include "game_object_renderer.hpp"

namespace doggo
{
    class ShipRenderer : public GameObjectRenderer
    {
        public:
            // Fields
            sf::Sprite sprite;
            const Ship &ship;

            // Constructor
            ShipRenderer(const sf::Texture &spriteTexture, const Ship &ship) ;

            // Methods
            virtual void update(float dt, const sf::Vector2f &b2Position, float b2Rotation);
            virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;

        private:
            // Fields
            PolygonRenderer outlineRenderer;
    };
} // doggo