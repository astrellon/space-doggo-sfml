#include "json_serialiser.hpp"

template <class T>
T tryGet(const json &input, const std::string &key, const T &defaultValue)
{
    auto value = input.find(key);
    if (value == input.end())
    {
        return defaultValue;
    }
    return value.value();
}

namespace doggo
{
    json JsonSerialiser::fromShip(const Ship &ship)
    {
        return {
            {"id", ship.id},
            {"shipName", ship.shipName},
            {"textureName", ship.textureName},
            {"physicsStats", fromPhysicsStats(ship.physicsObj.physicsStats)}
        };
    }

    std::unique_ptr<Ship> JsonSerialiser::toShip(const json &input)
    {
        auto id = input["id"].get<int>();
        auto shipName = input["shipName"].get<std::string>();
        auto textureName = input["textureName"].get<std::string>();
        const auto &physicsStats = toPhysicsStats(input["physicsStats"]);

        return std::make_unique<Ship>(id, shipName, textureName, physicsStats);
    }

    json JsonSerialiser::fromPolygon(const Polygon &polygon)
    {
        json polygonJson = json::array();

        for (auto &point : polygon.points)
        {
            polygonJson.push_back(fromVector(point));
        }

        return polygonJson;
    }

    Polygon JsonSerialiser::toPolygon(const json &input)
    {
        std::vector<sf::Vector2f> points;
        for (auto &point : input)
        {
            points.push_back(toVector(point));
        }

        return {points};
    }

    json JsonSerialiser::fromPhysicsStats(const PhysicsObjectStats &physicsStats)
    {
        return {
            {"mass", physicsStats.mass},
            {"polygon", fromPolygon(physicsStats.polygon)},
            {"offset", fromVector(physicsStats.offset)},
            {"scale", physicsStats.scale},
            {"position", fromVector(physicsStats.position)},
            {"rotation", physicsStats.rotation}
        };
    }

    PhysicsObjectStats JsonSerialiser::toPhysicsStats(const json &input)
    {
        auto mass = input["mass"].get<float>();
        auto scale = input["scale"].get<float>();
        auto polygon = toPolygon(input["polygon"]);
        auto offset = toVector(input["offset"]);
        auto position = toVector(input["position"]);
        auto rotation = input["rotation"].get<float>();

        return {mass, offset, scale, polygon, position, rotation};
    }

    json JsonSerialiser::fromVector(const sf::Vector2f &vec)
    {
        return {
            {"x", vec.x},
            {"y", vec.y}
        };
    }

    sf::Vector2f JsonSerialiser::toVector(const json &input)
    {
        auto x = input["x"].get<float>();
        auto y = input["y"].get<float>();

        return {x, y};
    }

    json JsonSerialiser::fromShipComponentDef(const ShipComponentDef &def)
    {
        json result = {
            {"id", def.id},
            {"name", def.name},
            {"energyNeeded", def.energyNeeded},
            {"size", def.size},
            {"isInternal", def.isInternal},
            {"type", "unknown"}
        };

        auto type = def.shipComponentType();
        if (type == ShipComponentEngine)
        {
            auto &engineDef = static_cast<const EngineComponentDef &>(def);
            result["force"] = engineDef.force;
            result["type"] = "engine";
        }
        else if (type == ShipComponentThruster)
        {
            auto &thrusterDef = static_cast<const ThrustersComponentDef &>(def);
            result["torque"] = thrusterDef.torque;
            result["type"] = "thruster";
        }
        else if (type == ShipComponentPowerSource)
        {
            auto &powerSourceDef = static_cast<const PowerSourceComponentDef &>(def);
            result["energyOutput"] = powerSourceDef.energyOutput;
            result["type"] = "powerSource";
        }

        return result;
    }

    std::unique_ptr<ShipComponentDef> JsonSerialiser::toShipComponentDef(const json &input)
    {
        auto id = input["id"].get<std::string>();
        auto name = input["name"].get<std::string>();
        auto energyNeeded = tryGet<float>(input, "energyNeeded", 0);
        auto size = input["size"].get<float>();
        auto type = input["type"].get<std::string>();
        auto isInternal = tryGet<bool>(input, "isInternal", false);

        if (type == "engine")
        {
            auto force = input["force"].get<float>();
            return std::make_unique<EngineComponentDef>(id, name, energyNeeded, size, force);
        }
        else if (type == "thruster")
        {
            auto torque = input["torque"].get<float>();
            return std::make_unique<ThrustersComponentDef>(id, name, energyNeeded, size, torque);
        }
        else if (type == "powerSource")
        {
            auto energyOutput = input["energyOutput"].get<float>();
            return std::make_unique<PowerSourceComponentDef>(id, name, size, energyOutput);
        }

        throw std::runtime_error("Unable to process ship component def json");
    }

    json JsonSerialiser::fromShipComponentDefManager(const ShipComponentDefManager &defManager)
    {
        auto defs = json::array();

        for (auto &pair : defManager.shipCompDefs)
        {
            defs.push_back(fromShipComponentDef(*pair.second));
        }

        return {
            {"defs", defs}
        };
    }

    std::unique_ptr<ShipComponentDefManager> JsonSerialiser::toShipComponentDefManager(const json &input)
    {
        auto result = std::make_unique<ShipComponentDefManager>();
        auto defsArray = input["defs"];

        for (auto &defJson : defsArray)
        {
            auto def = toShipComponentDef(defJson);
            result->shipCompDefs[def->id] = std::move(def);
        }

        return std::move(result);
    }

} // doggo
