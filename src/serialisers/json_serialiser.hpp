#pragma once

#include "../../thirdparty/json.hpp"

#include "../box2d/physics_object_stats.hpp"
#include "../ship_components/ship_component_def.hpp"
#include "../ship_components/ship_component_def_manager.hpp"
#include "../ship_components/engine_component_def.hpp"
#include "../ship_components/power_source_component_def.hpp"
#include "../ship_components/thrusters_component_def.hpp"
#include "../ship.hpp"
#include "../polygon.hpp"

using json = nlohmann::json;

namespace doggo
{
    class JsonSerialiser
    {
        public:
            // Methods
            static json fromShip(const Ship &ship);
            static std::unique_ptr<Ship> toShip(const json &input);

            static json fromPolygon(const Polygon &polygon);
            static Polygon toPolygon(const json &input);

            static json fromPhysicsStats(const PhysicsObjectStats &physicsStats);
            static PhysicsObjectStats toPhysicsStats(const json &input);

            static json fromVector(const sf::Vector2f &vec);
            static sf::Vector2f toVector(const json &input);

            static json fromShipComponentDef(const ShipComponentDef &def);
            static std::unique_ptr<ShipComponentDef> toShipComponentDef(const json &input);

            static json fromShipComponentDefManager(const ShipComponentDefManager &defManager);
            static std::unique_ptr<ShipComponentDefManager> toShipComponentDefManager(const json &input);

            static json fromHardpoint(const Hardpoint &hardpoint);
    };
} // doggo