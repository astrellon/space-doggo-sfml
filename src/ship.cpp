#include "ship.hpp"

#include <iostream>

#include "utils/utils.hpp"

namespace doggo
{
    Ship::Ship(int id, const std::string &shipName, const std::string &textureName, const PhysicsObjectStats &physicsStats) :
        GameObject(id, physicsStats),
        shipName(shipName),
        textureName(textureName),
        moveDirection(NoMove),
        rotateDirection(NoRotate)
    {

    }

    void Ship::move(MoveDirection direction)
    {
        this->moveDirection = direction;
    }

    void Ship::rotate(RotateDirection rotation)
    {
        this->rotateDirection = rotation;
    }

    void Ship::update(float dt)
    {
        this->preUpdate(dt);

        auto powerPercent = Utils::clamp01(this->powerAvailable / this->powerNeeds);

        for (auto &&pair : this->hardpoints)
        {
            auto comp = pair.second->getEquipped();
            if (comp == nullptr || !comp->isActive())
            {
                continue;
            }

            comp->update(dt, powerPercent);
        }
    }

    void Ship::preUpdate(float dt)
    {
        this->powerAvailable = 0.0f;
        this->powerNeeds = 0.0f;

        for (auto &&pair : this->hardpoints)
        {
            auto comp = pair.second->getEquipped();
            if (comp == nullptr || !comp->isActive())
            {
                continue;
            }

            auto &def = comp->def;
            this->powerNeeds += def.energyNeeded;

            comp->preUpdate(dt);
        }
    }

    Hardpoint &Ship::addHardpoint(const std::string &name, const sf::Vector2f &position, float size)
    {
        auto result = new Hardpoint(name, position, size, *this);
        this->hardpoints.emplace(name, result);
        return *result;
    }

    Hardpoint &Ship::getHardpoint(const std::string &name)
    {
        return *this->hardpoints[name].get();
    }

    void Ship::addAvailablePower(float power)
    {
        this->powerAvailable += power;
    }
}