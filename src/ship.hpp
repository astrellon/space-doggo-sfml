#pragma once

#include <vector>
#include <map>

#include <SFML/System.hpp>

#include <Box2D/Box2D.h>

#include "ship_components/ship_component.hpp"
#include "power_needs.hpp"
#include "hardpoint.hpp"
#include "box2d/physics_object.hpp"
#include "box2d/physics_object_stats.hpp"
#include "game_object.hpp"
#include "polygon.hpp"

namespace doggo
{
    enum MoveDirection { NoMove, Forwards, Backwards, Left, Right };
    enum RotateDirection { NoRotate, Clockwise, CounterClockwise };

    class Ship : public GameObject
    {
        public:
            // Fields
            const std::string textureName;

            std::string shipName;
            float powerAvailable;
            float powerNeeds;

            std::map<std::string, std::unique_ptr<Hardpoint>> hardpoints;
            MoveDirection moveDirection;
            RotateDirection rotateDirection;

            // Constructor
            Ship(int id, const std::string &shipName, const std::string &textureName, const PhysicsObjectStats &physicsStats);

            // Methods
            void move(MoveDirection direction);
            void rotate(RotateDirection rotation);

            virtual void update(float dt);
            virtual RendererType rendererType() { return RenderShip; }
            void preUpdate(float dt);

            Hardpoint &addHardpoint(const std::string &name, const sf::Vector2f &position, float size);
            Hardpoint &getHardpoint(const std::string &name);

            void addAvailablePower(float amount);


    };
} // doggo
