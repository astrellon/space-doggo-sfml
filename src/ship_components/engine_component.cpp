#include "engine_component.hpp"

#include <Box2D/Box2D.h>

#include "../ship.hpp"
#include "../utils/utils.hpp"

namespace doggo
{
    EngineComponent::EngineComponent(int id, const EngineComponentDef &def) :
        ShipComponent(id, def),
        engineDef(def)
    {

    }

    void EngineComponent::update(float dt, float powerPercent)
    {
        auto &ship = this->parent->parent;
        auto movement = ship.moveDirection;
        if (movement != Forwards && movement != Backwards)
        {
            return;
        }

        auto multiplier = movement == Forwards ? -powerPercent : powerPercent;
        auto force = multiplier * this->engineDef.force;
        auto transformForce = Utils::rotateVectorRad(sf::Vector2f(0, force), ship.physicsObj.body->GetAngle());
        ship.physicsObj.body->ApplyForceToCenter(b2Vec2(transformForce.x * 0.001f, transformForce.y * 0.001f), true);
    }
} // doggo
