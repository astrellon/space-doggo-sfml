#pragma once

#include "ship_component.hpp"
#include "engine_component_def.hpp"

namespace doggo
{
    class EngineComponent : public ShipComponent
    {
        public:
            // Fields
            const EngineComponentDef &engineDef;

            // Constructor
            EngineComponent(int id, const EngineComponentDef &def);

            // Methods
            virtual void update(float dt, float powerPercent);
    };
} // doggo