#include "engine_component_def.hpp"

namespace doggo
{
    EngineComponentDef::EngineComponentDef(const std::string &id, const std::string &name, float energyNeeded, float size, float force) :
        ShipComponentDef(id, name, energyNeeded, size, false),
        force(force)
    {

    }
} // doggo
