#pragma once

#include "ship_component_def.hpp"

namespace doggo
{
    class EngineComponentDef : public ShipComponentDef
    {
        public:
            // Fields
            const float force;

            // Constructor
            EngineComponentDef(const std::string &id, const std::string &name, float energyNeeded, float size, float force);

            // Methods
            virtual ShipComponentType shipComponentType() const
            {
                return ShipComponentEngine;
            }
    };
} // doggo