#include "power_source_component.hpp"

#include "../ship.hpp"

namespace doggo
{
    PowerSourceComponent::PowerSourceComponent(int id, const PowerSourceComponentDef &def) :
        ShipComponent(id, def),
        powerSourceDef(def)
    {

    }

    void PowerSourceComponent::preUpdate(float dt)
    {
        auto &ship = this->parent->parent;
        ship.addAvailablePower(this->powerSourceDef.energyOutput);
    }
} // doggo
