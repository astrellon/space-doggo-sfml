#pragma once

#include "ship_component.hpp"
#include "power_source_component_def.hpp"

namespace doggo
{
    class PowerSourceComponent : public ShipComponent
    {
        public:
            // Fields
            const PowerSourceComponentDef &powerSourceDef;

            // Constructor
            PowerSourceComponent(int id, const PowerSourceComponentDef &def);

            // Methods
            virtual void preUpdate(float dt);
    };
} // doggo