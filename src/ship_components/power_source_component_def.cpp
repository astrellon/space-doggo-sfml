#include "power_source_component_def.hpp"

namespace doggo
{
    PowerSourceComponentDef::PowerSourceComponentDef(const std::string &id, const std::string &name, float size, float energyOutput) :
        ShipComponentDef(id, name, 0.0f, size, true),
        energyOutput(energyOutput)
    {

    }
} // doggo
