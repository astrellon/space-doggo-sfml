#pragma once

#include "ship_component_def.hpp"

namespace doggo
{
    class PowerSourceComponentDef : public ShipComponentDef
    {
        public:
            // Fields
            const float energyOutput;

            // Constructor
            PowerSourceComponentDef(const std::string &id, const std::string &name, float size, float energyOutput);

            // Methods
            virtual ShipComponentType shipComponentType() const
            {
                return ShipComponentPowerSource;
            }
    };
} // doggo