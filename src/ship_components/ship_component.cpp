#include "ship_component.hpp"

namespace doggo
{
    ShipComponent::ShipComponent(int id, const ShipComponentDef &def) :
        id(id),
        def(def),
        parent(nullptr),
        active(true)
    {

    }

    void ShipComponent::preUpdate(float dt)
    {

    }
    void ShipComponent::update(float dt, float powerPercent)
    {

    }
} // doggo
