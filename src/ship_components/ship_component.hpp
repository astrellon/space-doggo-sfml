#pragma once

#include <vector>

#include "ship_component_def.hpp"
#include "../non_copyable.hpp"

namespace doggo
{
    class Hardpoint;

    class ShipComponent : public NonCopyable
    {
        public:
            // Fields
            const int id;
            const ShipComponentDef &def;
            Hardpoint *parent;

            // Constructor
            ShipComponent(int id, const ShipComponentDef &def);

            // Methods
            inline bool isActive() const
            {
                return this->active;
            }
            inline void setActive(bool active)
            {
                this->active = active;
            }

            virtual void preUpdate(float dt);
            virtual void update(float dt, float powerPercent);

        private:
            // Fields
            bool active;
    };
} // doggo
