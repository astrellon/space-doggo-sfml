#include "ship_component_def.hpp"

namespace doggo
{
    ShipComponentDef::ShipComponentDef(const std::string &id, const std::string &name, float energyNeeded, float size, bool isInternal) :
        id(id),
        name(name),
        energyNeeded(energyNeeded),
        size(size),
        isInternal(isInternal)
    {

    }
}