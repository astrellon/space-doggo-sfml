#pragma once

#include <string>
#include <SFML/System.hpp>

#include "../non_copyable.hpp"

namespace doggo
{
    enum ShipComponentType
    {
        ShipComponentUnknown,
        ShipComponentEngine,
        ShipComponentThruster,
        ShipComponentPowerSource
    };

    class ShipComponentDef : public NonCopyable
    {
        public:
            // Fields
            const std::string id;
            const std::string name;
            const float energyNeeded;
            const float size;
            const bool isInternal;

            // Constructor
            ShipComponentDef(const std::string &id, const std::string & name, float energyNeeded, float size, bool isInternal);

            // Methods
            virtual ShipComponentType shipComponentType() const
            {
                return ShipComponentUnknown;
            }

        private:
    };
} // doggo
