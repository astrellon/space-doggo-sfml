#include "ship_component_def_manager.hpp"

namespace doggo
{
    void ShipComponentDefManager::registerDef(std::unique_ptr<ShipComponentDef> def)
    {
        this->shipCompDefs.emplace(def->id, std::move(def));
    }
} // doggo
