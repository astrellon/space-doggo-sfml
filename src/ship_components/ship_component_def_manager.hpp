#pragma once

#include <map>
#include <memory>

#include "ship_component_def.hpp"

namespace doggo
{
    class ShipComponentDefManager
    {
        public:
            // Fields
            std::map<std::string, std::unique_ptr<ShipComponentDef>> shipCompDefs;

            // Constructor

            // Methods
            void registerDef(std::unique_ptr<ShipComponentDef> def);
    };
} // doggo