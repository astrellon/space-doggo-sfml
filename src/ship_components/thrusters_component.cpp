#include "thrusters_component.hpp"

#include <iostream>

#include <Box2D/Box2D.h>

#include "../ship.hpp"
#include "../utils/utils.hpp"

namespace doggo
{
    ThrustersComponent::ThrustersComponent(int id, const ThrustersComponentDef &def) :
        ShipComponent(id, def),
        thrustersDef(def)
    {

    }

    void ThrustersComponent::update(float dt, float powerPercent)
    {
        auto &ship = this->parent->parent;
        auto rotation = ship.rotateDirection;
        auto movement = ship.moveDirection;

        if (rotation != NoRotate)
        {
            auto multiplier = rotation == Clockwise ? powerPercent : -powerPercent;
            auto force = multiplier * this->thrustersDef.torque;
            ship.physicsObj.body->ApplyTorque(force * 0.001f, true);
        }

        if (movement == Left || movement == Right)
        {
            auto multiplier = movement == Left ? -powerPercent : powerPercent;
            auto force = multiplier * this->thrustersDef.torque;
            auto transformForce = Utils::rotateVectorRad(sf::Vector2f(force, 0), ship.physicsObj.body->GetAngle());
            ship.physicsObj.body->ApplyForceToCenter(b2Vec2(transformForce.x * 0.001f, transformForce.y * 0.001f), true);
        }
    }
} // doggo
