#pragma once

#include "ship_component.hpp"
#include "thrusters_component_def.hpp"

namespace doggo
{
    class ThrustersComponent : public ShipComponent
    {
        public:
            // Fields
            const ThrustersComponentDef &thrustersDef;

            // Constructor
            ThrustersComponent(int id, const ThrustersComponentDef &def);

            // Methods
            virtual void update(float dt, float powerPercent);
    };
} // doggo