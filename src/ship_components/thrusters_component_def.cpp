#include "thrusters_component_def.hpp"

namespace doggo
{
    ThrustersComponentDef::ThrustersComponentDef(const std::string &id, const std::string &name, float energyNeeded, float size, float torque) :
        ShipComponentDef(id, name, energyNeeded, size, false),
        torque(torque)
    {

    }
} // doggo
