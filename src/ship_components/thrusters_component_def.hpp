#pragma once

#include "ship_component_def.hpp"

namespace doggo
{
    class ThrustersComponentDef : public ShipComponentDef
    {
        public:
            // Fields
            const float torque;

            // Constructor
            ThrustersComponentDef(const std::string &id, const std::string &name, float energyNeeded, float size, float torque);

            // Methods
            virtual ShipComponentType shipComponentType() const
            {
                return ShipComponentThruster;
            }
    };
} // doggo