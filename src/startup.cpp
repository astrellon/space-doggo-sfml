#include "startup.hpp"

namespace doggo
{
    void Startup::loadTextures(GameManager &gameManager)
    {
        auto &tm = gameManager.textureManager;

        tm.loadTexture("corgi", "corgi-photo.jpg");
        tm.loadTexture("goodDoggo", "goodDoggo.png");
        tm.loadTexture("ship1", "ship1.png");
        tm.loadTexture("background", "background.png");
        tm.loadTexture("background2", "background2.png");
    }
} // doggo
