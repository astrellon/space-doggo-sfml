#pragma once

#include "game_manager.hpp"

namespace doggo
{
    class Startup
    {
        public:
            static void loadTextures(GameManager &gameManager);
    };
} // doggo