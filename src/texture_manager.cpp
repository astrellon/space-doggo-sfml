#include "texture_manager.hpp"

#include <sstream>
#include <iostream>

namespace doggo
{
    bool TextureManager::loadTexture(const std::string &name, const std::string &filename)
    {
        if (name.empty())
        {
            return false;
        }

        auto fullPath = this->rootFolder + filename;
        auto texture = new sf::Texture();
        if (!texture->loadFromFile(fullPath))
        {
            return false;
        }

        texture->setSmooth(false);
        texture->setRepeated(true);
        this->textures.emplace(name, texture);
        return true;
    }

    const sf::Texture *TextureManager::texture(const std::string &name) const
    {
        auto find = this->textures.find(name);
        if (find == this->textures.end())
        {
            return nullptr;
        }

        return find->second.get();
    }

    std::string TextureManager::textureName(const sf::Texture *texture) const
    {
        for (const auto &iter : this->textures)
        {
            if (iter.second.get() == texture)
            {
                return iter.first;
            }
        }

        return std::string();
    }
}
