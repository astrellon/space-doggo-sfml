#pragma once

#include <SFML/Graphics.hpp>

#include <string>
#include <map>
#include <memory>

#include "non_copyable.hpp"

namespace doggo
{
    class TextureManager : public NonCopyable
    {
        public:
            typedef std::map<std::string, std::unique_ptr<sf::Texture> > TextureMap;
            std::string rootFolder;

            // Methods
            bool loadTexture(const std::string &name, const std::string &filename);
            const sf::Texture *texture(const std::string &name) const;
            std::string textureName(const sf::Texture *texture) const;

        private:
            // Fields
            TextureMap textures;
    };
}
