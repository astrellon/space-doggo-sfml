#pragma once

#define DEG_TO_RAD 0.01745329251994329576923690768489f
#define RAD_TO_DEG 57.295779513082320876798154814105f
#define FROM_BOX2D 100.0f
#define TO_BOX2D 0.01f

#include <math.h>

#include <SFML/System.hpp>
#include <Box2D/Box2D.h>

namespace doggo
{
    class Utils
    {
        public:
            // Methods
            inline static float clamp01(float input)
            {
                return input < 0.0f ? 0.0f : (input > 1.0f ? 1.0f : input);
            }

            inline static float sindeg(float degree)
            {
                return sin(degree * DEG_TO_RAD);
            }
            inline static float cosdeg(float degree)
            {
                return cos(degree * DEG_TO_RAD);
            }

            inline static sf::Vector2f rotateVectorDeg(const sf::Vector2f &input, float degrees)
            {
                float cs = cosdeg(degrees);
                float sn = sindeg(degrees);

                float x = input.x * cs - input.y * sn;
                float y = input.x * sn + input.y * cs;
                return sf::Vector2f(x, y);
            }

            inline static sf::Vector2f rotateVectorRad(const sf::Vector2f &input, float radians)
            {
                float cs = cos(radians);
                float sn = sin(radians);

                float x = input.x * cs - input.y * sn;
                float y = input.x * sn + input.y * cs;
                return sf::Vector2f(x, y);
            }

            inline static sf::Vector2f normalise(const sf::Vector2f &input)
            {
                auto invMag = 1.0f / sqrt(input.x * input.x + input.y * input.y);
                return {input.x * invMag, input.y * invMag};
            }

            inline static float dot(const sf::Vector2f &a, const sf::Vector2f &b)
            {
                return a.x * b.x + a.y * b.y;
            }

            inline static b2Vec2 toBox2d(const sf::Vector2f &input)
            {
                return {input.x * TO_BOX2D, input.y * TO_BOX2D};
            }

            inline static sf::Vector2f fromBox2d(const b2Vec2 &input)
            {
                return {input.x * FROM_BOX2D, input.y * FROM_BOX2D};
            }

            inline static sf::Vector2f min(const sf::Vector2f &a, const sf::Vector2f &b)
            {
                return {Utils::min(a.x, b.x), Utils::min(a.y, b.y)};
            }
            inline static sf::Vector2f max(const sf::Vector2f &a, const sf::Vector2f &b)
            {
                return {Utils::max(a.x, b.x), Utils::max(a.y, b.y)};
            }

            inline static float min(float a, float b)
            {
                return a < b ? a : b;
            }
            inline static float max(float a, float b)
            {
                return a < b ? b : a;
            }
    };
} // doggo